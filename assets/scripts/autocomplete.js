
function createTemplate() {
    return `
    <label for="">Search For Movie</label>
    <input type="input" class="input" id="search-field">
    <div class="dropdown" id="auto-complete-dropdown">
    <div class="dropdown-menu" id="" role="menu">
    <div class="dropdown-content results" id="target">
    
    
    </div>
    </div>
    </div>
    `
}

function createAutoComplete({ root, fetchData, listInnerHtml, onListClick }) {
    root.innerHTML = createTemplate();
    const searchField = root.querySelector("#search-field");
    const autoCompleteDropDown = root.querySelector("#auto-complete-dropdown")
    let target = root.querySelector("#target")


    const loadItems = async function (event) {
        autoCompleteDropDown.classList.add("is-active");
        target.innerHTML = "";

        const items = await fetchData(event);
        console.log(items);


        items.forEach(item => {


            const a = document.createElement("a");

            a.innerHTML = listInnerHtml(item);

            a.classList.add('dropdown-item');
            a.addEventListener('click', () => {
                autoCompleteDropDown.classList.remove("is-active");
                searchField.value = item.Title;
            })
            a.addEventListener('click', (event) => {
                console.log(event);

                onListClick(item.imdbID);

            })

            target.appendChild(a);
        });

    }



    searchField.addEventListener('keyup', debounce(loadItems));




    document.addEventListener('click', (event) => {
        if (!root.contains(event.target)) {
            autoCompleteDropDown.classList.remove("is-active");
            if (!searchField.value)
                searchField.value = "";
        }

    })
}
