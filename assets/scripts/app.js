const yourKey = "3f555c3d";
const te = document.getElementById('template-for-summary');
console.log(te);

console.log(typeof (te.dataset.info));



const template = document.getElementById('template-for-summary')

const summaryHtml = template.content.cloneNode(true);
console.log(summaryHtml);



createAutoComplete({
    root: document.getElementById("auto-complete1"),

    fetchData: async function (event) {
        let items = await fetch("s", event.target.value);
        return items;
    },

    listInnerHtml: function (item) {
        return `
        <img src= ${item.Poster == "N/A" ? "" : item.Poster}>
                <h3>${item.Title}</h3>`
    },

    onListClick: async function (itemId) {

        console.log('hey');

        let output = document.getElementById('left-summary');
        onMovieSelect(itemId, output, "left")


    }


});



createAutoComplete({
    root: document.getElementById("auto-complete2"),

    fetchData: async function (event) {
        let items = await fetch("s", event.target.value);
        return items;
    },

    listInnerHtml: function (item) {
        return `
        <img src= ${item.Poster == "N/A" ? "" : item.Poster}>
                <h3>${item.Title}</h3>`
    },

    onListClick: async function (itemId) {

        console.log('hey');

        let output = document.getElementById('right-summary');
        onMovieSelect(itemId, output, "right")


    }


});


let leftSide;
let rightSide;
async function onMovieSelect(itemId, output, side) {
    console.log(side);

    let item = await fetch("i", itemId);
    createSummary(output, item)
    if (side == "left")
        leftSide = item;
    if (side == "right")
        rightSide = item;

    console.log(leftSide);
    console.log(rightSide);


    if (leftSide && rightSide) {
        console.log("now we can compare");
        compareMovies();

    }




}

function compareMovies() {
    const leftSideStats = document.querySelectorAll("#left-summary .notification")
    const rightSideStats = document.querySelectorAll("#right-summary .notification")

    leftSideStats.forEach((leftStat, index) => {



        const rightStat = rightSideStats[index];
        let leftSideValue = leftStat.dataset.value.indexOf('.' != -1) ? parseFloat(leftStat.dataset.value) : parseInt(leftStat.dataset.value);
        let rightSideValue = leftStat.dataset.value.indexOf('.' != -1) ? parseFloat
            (rightStat.dataset.value) : parseInt(rightStat.dataset.value);







        console.log(leftSideValue, rightSideValue);

        console.log(typeof (leftSideValue));

        console.log('poe');




        if (leftSideValue < rightSideValue) {
            console.log("left is small");

            leftStat.classList.remove('is-primary');
            leftStat.classList.add('is-warning');
        }
        else {

            console.log('right is small');

            rightStat.classList.remove('is-primary');
            rightStat.classList.add('is-warning');
        }



    })



}

const fetch = function (option, searchText) {
    const promise = new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', `http://omdbapi.com/?${option}=${searchText}&apikey=${yourKey}`);
        xhr.onload = () => {

            const response = JSON.parse(xhr.response);


            if (response.Error)
                return [];

            if (option == "s")
                resolve(response.Search);
            if (option == "i")
                resolve(response)
        }
        xhr.send();
    })

    return promise;

}

function createSummary(outputElement, item) {

    console.log('create');

    console.log(outputElement, item);


    let dollars = item.BoxOffice.replace(/\$/g, "").replace(/,/g, "");
    dollars = parseInt(dollars);

    const metaScore = parseInt(item.Metascore);

    const imdbRatings = parseFloat(item.imdbRating.replace(/\,/g, ""))

    let awardCounts = 0;
    let awards = item.Awards.split(' ').forEach(word => {
        if (isNaN(word))
            return

        awardCounts = awardCounts + parseInt(word);

        console.log(typeof (dollars));


    });
    ;

    outputElement.innerHTML =
        `<article class="media">
        <figure class="media-left">
    
            <p class="image">
                <img src="${item.Poster}" alt="">
            </p>
        </figure>
        <div class="media-content content">
            <h1>${item.Title}</h1>
            <h4>${item.Genre}</h4>
            <p>${item.Plot}</p>
        </div>
    </article>
    
    <article data-value=${awardCounts} class="notification is-primary data">
        <p class="title">${item.Awards}</p>
        <p class= "subtitle"> Awards</p>
    </article>
    
    <article data-value=${metaScore} class="notification is-primary">
        <p class="title">${item.Metascore}</p>
        <p class= "subtitle">Metascore</p>
    </article>

    <article data-value=${dollars} class="notification is-primary">
    <p class="title">${item.BoxOffice}</p>
    <p class= "subtitle"> BoxOffice</p>
    </article>

    <article data-value=${imdbRatings} class="notification is-primary">
    <p class="title">${item.imdbRating}</p>
    <p class= "subtitle"> Ratings</p>
    </article>
   
`;
}

