const debounce = function (callback) {
    let timerId;
    return function (...args) {
        if (timerId)
            clearTimeout(timerId);

        timerId = setTimeout(() => { callback.apply(null, args) }, 400);

    }
}




